import java.util.Scanner;

class Main {
  public static void main(String[] args) 
  {
    Scanner sc= new Scanner(System.in);
    
    //Players names
    System.out.println("Player 1 enter your name");
    String p1=sc.nextLine();
    System.out.println("Player 2 enter your name");
    String p2=sc.nextLine();
    
    //Create a board
    
    char board[][] = new char[3][3];
    
    //Fill the board
    
    for(int i=0;i<3;i++)
    {
      for(int j=0;j<3;j++)
      {
        board[i][j]='-';
	  }
	}
    
    //Keep track of whose turn it is
    boolean isplayer1 = true;
    
    //Keep track if the game has ended
    boolean gameEnded=false;
    
    while(!gameEnded)
    {
      //draw the board
      drawboard(board);

      //Print out players turn
      if(isplayer1)
        System.out.println(p1 + "'s turn");
      else
         System.out.println(p2 + "'s turn");


      //Keep tack of what symbol we are using to play
      char symbol = ' ';
      if(isplayer1)
        symbol='x';
      else
        symbol='o';

      //Get row and col from user
      int row=0;
      int col=0;
      while(true)
      {
        System.out.println("Enter the row(0,1 or 2)");
        row=sc.nextInt();
        System.out.println("Enter the col(0,1 or 2)");
        col=sc.nextInt();

        //If row and col are valid
        if(row<0 || row>2 || col<0 || col>2)
          System.out.println("Invalid positions/Out of bounds");
        //If board position has o and x already
        else if(board[row][col]!='-')
          System.out.println("Someone has already made a move there");
        else
        {
          break;
        }
      }
      board[row][col]=symbol;

      //check if a player has won
      if(hasWon(board)=='x')
      {
		System.out.println(p1 + " has won");
        gameEnded=true;
      }
        
      else if(hasWon(board)=='o')
      {
		System.out.println(p2 + " has won");
        gameEnded=true;
      }
        
      else
      {
        if(hasTied(board))
        {
		  System.out.println("Tied");
          gameEnded=true;
        }
        
        else
          //Continue the game
		  isplayer1 =!isplayer1;
      }
    }
    
	drawboard(board);
  }
  
  public static void drawboard(char[][] board)
  {
    for(int i=0;i<3;i++)
    {
      for(int j=0;j<3;j++)
      {
        System.out.print(board[i][j]);
	  }
      System.out.println();
	}
  }
  
  public static char hasWon(char[][] board)
  {
    //rows
    for(int i=0;i<3;i++) 
    {
	  if(board[i][0]==board[i][1] && board[i][1]==board[i][2] && board[i][0]==board[i][2] && board[i][0]!='-')
        return board[i][0];
    }
    
    //cols
    for(int j=0;j<3;j++)
    {
	  if(board[0][j]==board[1][j] && board[1][j]==board[2][j] && board[0][j]==board[2][j] && board[0][j]!='-')
        return board[0][j];
    }
    
    //diagonals
    if(board[0][0]==board[1][1] && board[1][1]==board[2][2] && board[0][0]==board[2][2] && board[0][0]!='-')
      return board[0][0];
    
    if(board[2][0]==board[1][1] && board[1][1]==board[0][2] && board[0][2]==board[2][0] && board[2][0]!='-')
      return board[0][0];
    
    //nobody has won
    return '-';
  }
  
  //Check if the board is full
  public static boolean hasTied(char[][] board)
  {
	for(int i=0;i<3;i++)
    {
      for(int j=0;j<3;j++)
      {
		if(board[i][j]=='-')
          return false;
      }
    }
    return true;
  }
}